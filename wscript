def options(opt):
    opt.load('compiler_cxx')

def configure(conf):
    conf.load('compiler_cxx')
def build(bld):
    bld.program(
        features     = ['cxx','cprogram'],
        source       = ['main.cxx',
                        'initial.cxx',
                        'compute_coefficients/compute_coefficients.cxx',
                        'compute_coefficients/compute_Cxyz.cxx',
                        'compute_coefficients/compute_Cp.cxx',
                        'compute_coefficients/simplified_Rx.cxx',
                        'compute_coefficients/simplified_Ry.cxx',
                        'compute_coefficients/simplified_Rp.cxx',
                        'compute_coefficients/compute_v_on_interface/compute_v_on_interface.cxx',
                        'compute_coefficients/compute_v_on_interface/compute_norm.cxx',
                        'compute_coefficients/compute_v_on_interface/compute_dv_dtt.cxx',
                        'compute_coefficients/compute_jumps.cxx',
                        'compute_coefficients/Interface/Interface.cxx'],

        target       = 'Gamr_disc',
        # cxxflags      = ['-std=c++0x','-g','-Wall','-Drestrict=','-DFTENSOR_DEBUG'],
        cxxflags      = ['-std=c++0x','-O3','-Wall','-Drestrict='],
        # cxxflags      = ['-std=c++0x','-O3','-Wall','-Drestrict=','-DNDEBUG'],
        # cxxflags      = ['-std=c++0x','-O3','-Wall','-Drestrict=','-pg'],
        # linkflags = ['-pg'],
        lib = ['boost_filesystem', 'boost_system', 'gsl', 'gslcblas'],
        includes = ['FTensor'],
        )
