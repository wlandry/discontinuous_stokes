#ifndef GAMR_CONSTANTS_HXX
#define GAMR_CONSTANTS_HXX

const int N(64);
const int Nx(N);
const int Ny(N);
const double min_eta=1;
const double max_eta=1e10;
#include <cmath>
const double log_max_eta=std::log(max_eta);
const double log_min_eta=std::log(min_eta);
// const double middle=(25 - 0.00000001)/64;
// const double middle=(25 + 0.00000001)/64 + 1/(2*Nx);
// const double middle=(25 - 1/32.0)/64;
const double middle=0.4;
const double h(1.0/N);
const double pi(atan(1.0)*4);
const int max_r=4;

const double r2_inclusion=0.1;
const double epsilon=1;

#endif
