#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "Model.hxx"
#include "constants.hxx"
#include "write_vtk.hxx"
#include "Coefficient.hxx"

extern void initial(const Model &model, double zx[Nx+1][Ny], double zy[Nx][Ny+1],
                    double log_etax[Nx+1][Ny], double log_etay[Nx][Ny+1],
                    double eta_cell[Nx][Ny],
                    double p[Nx][Ny], double fx[Nx+1][Ny], double fy[Nx][Ny+1],
                    double distx[Nx+1][Ny], double disty[Nx][Ny+1],
                    double dist_cell[Nx][Ny],
                    double dist_edge[Nx+1][Ny+1]);

void compute_coefficients(const double log_etax[Nx+1][Ny],
                          const double log_etay[Nx][Ny+1],
                          const double eta_cell[Nx][Ny],
                          const double distx[Nx+1][Ny],
                          const double disty[Nx][Ny+1],
                          const double dist_cell[Nx][Ny],
                          const double dist_edge[Nx+1][Ny+1],
                          std::vector<Coefficient> Cfx[2][Nx+1][Ny],
                          std::vector<Coefficient> Cfy[2][Nx][Ny+1],
                          std::vector<Coefficient> Cfp[2][Nx][Ny],
                          double dRx[Nx+1][Ny],
                          double dRy[Nx][Ny+1]);

int main()
{
  /* Eta is collocated with z.  Maybe eta should be cell & vertex
     centered?  It would simplify most of these differences, but it
     would make dlog_etaxy for the momentum residual and dlog_etax for
     the continuity residual more messy.  Also, with it collocated
     with z, it is easy to compute v=z/eta. */

  double zx[Nx+1][Ny], zy[Nx][Ny+1], log_etax[Nx+1][Ny], log_etay[Nx][Ny+1],
    eta_cell[Nx][Ny], p[Nx][Ny], dp[Nx][Ny], fx[Nx+1][Ny], fy[Nx][Ny+1],
    distx[Nx+1][Ny], disty[Nx][Ny+1], dist_cell[Nx][Ny], dist_edge[Nx+1][Ny+1],
    dRx[Nx+1][Ny], dRy[Nx][Ny+1];
  double zx_new[Nx+1][Ny], zy_new[Nx][Ny+1], p_new[Nx][Ny];

  std::vector<Coefficient> Cfx[2][Nx+1][Ny], Cfy[2][Nx][Ny+1], Cfp[2][Nx][Ny];

  const bool check_initial(false);
  const bool jacobi(check_initial);
  /* Initial conditions */

  const int n_sweeps(check_initial ? 1 : 1000001);
  const double theta_mom=0.7;
  const double theta_continuity=1.0;
  const double tolerance=1.0e-6;

  const int output_interval=1000;

  Model model(Model::inclusion);

  initial(model,zx,zy,log_etax,log_etay,eta_cell,p,fx,fy,
          distx,disty,dist_cell,dist_edge);

  compute_coefficients(log_etax,log_etay,eta_cell,distx,disty,dist_cell,
                       dist_edge,Cfx,Cfy,Cfp,dRx,dRy);

  double Resid_p[Nx][Ny], Resid_x[Nx+1][Ny], Resid_y[Nx][Ny+1];

  for(int i=0;i<Nx+1;++i)
    for(int j=0;j<Ny;++j)
      {
        if(!check_initial && i!=0 && i!=Nx)
          zx[i][j]=0;
        Resid_x[i][j]=0;
      }

  for(int i=0;i<Nx;++i)
    for(int j=0;j<Ny+1;++j)
      {
        if(!check_initial && j!=0 && j!=Ny)
          zy[i][j]=0;
        Resid_y[i][j]=0;
      }

  for(int i=0;i<Nx;++i)
    for(int j=0;j<Ny;++j)
      {
        if(!check_initial)
          p[i][j]=0;
        Resid_p[i][j]=0;
      }

  for(int sweep=0;sweep<n_sweeps;++sweep)
    {
      /* zx */
      for(int rb=0;rb<2;++rb)
        {
          for(int j=0;j<Ny;++j)
            {
              int i_min=(j+rb)%2;
              for(int i=i_min;i<Nx+1;i+=2)
                {
                  if(i!=0 && i!=Nx)
                    {
                      double dp_x=(p[i][j]-p[i-1][j])/h;

                      double Rx(0);
                      for(auto &c: Cfx[0][i][j])
                        Rx+=zx[c.i][c.j]*c.val;
                      for(auto &c: Cfx[1][i][j])
                        Rx+=zy[c.i][c.j]*c.val;
                      Rx-=dp_x + fx[i][j];

                      Resid_x[i][j]=Rx;
                      if(!jacobi)
                        zx[i][j]-=theta_mom*Rx/dRx[i][j];
                      else
                        zx_new[i][j]=zx[i][j]-theta_mom*Rx/dRx[i][j];
                    }
                  else
                    {
                      if(jacobi)
                        zx_new[i][j]=zx[i][j];
                    }
                }
            }
        }

      if(sweep%output_interval==0)
        {
          std::stringstream ss;
          ss << "zx_resid" << sweep;
          write_vtk(ss.str(),Nx+1,N,Resid_x);
        }
      /* zy */

      for(int rb=0;rb<2;++rb)
        {
          for(int j=0;j<Ny+1;++j)
            {
              int i_min=(j+rb)%2;
              for(int i=i_min;i<Nx;i+=2)
                {
                  if(j!=0 && j!=Ny)
                    {
                      double dp_y=(p[i][j]-p[i][j-1])/h;

                      double Ry(0);
                      for(auto &c: Cfy[0][i][j])
                        Ry+=zx[c.i][c.j]*c.val;
                      for(auto &c: Cfy[1][i][j])
                        Ry+=zy[c.i][c.j]*c.val;
                      Ry-=dp_y + fy[i][j];

                      Resid_y[i][j]=Ry;
                      if(!jacobi)
                        zy[i][j]-=theta_mom*Ry/dRy[i][j];
                      else
                        zy_new[i][j]=zy[i][j]-theta_mom*Ry/dRy[i][j];
                    }
                  else
                    {
                      if(jacobi)
                        zy_new[i][j]=zy[i][j];
                    }
                }
            }
        }

      if(sweep%output_interval==0)
        {
          std::stringstream ss;
          ss << "zy_resid" << sweep;
          write_vtk(ss.str(),Nx,N,Resid_y);
        }

      /* Pressure update */

      for(int j=0;j<Ny;++j)
        for(int i=0;i<Nx;++i)
          {
            double Rc(0);
            for(auto &c: Cfp[0][i][j])
              Rc+=zx[c.i][c.j]*c.val;
            for(auto &c: Cfp[1][i][j])
              Rc+=zy[c.i][c.j]*c.val;

            double dRmp_dp=-1/h;
            double dRmm_dp=1/h;

            double dRc_dp(0);
            if(i!=Nx-1)
              dRc_dp+=(1/h)*dRmp_dp/dRx[i+1][j];

            if(i!=0)
              dRc_dp+=(-1/h)*dRmm_dp/dRx[i][j];

            if(j!=Ny-1)
              dRc_dp+=(1/h)*dRmp_dp/dRy[i][j+1];

            if(j!=0)
              dRc_dp+=(-1/h)*dRmm_dp/dRy[i][j];

            // if(i!=Nx-1)
            //   dRc_dp+=(1/h - dlog_etax/2)*dRmp_dp/dRx[i+1][j];

            // if(i!=0)
            //   dRc_dp+=(-1/h - dlog_etax/2)*dRmm_dp/dRx[i][j];

            // if(j!=Ny-1)
            //   dRc_dp+=(1/h - dlog_etay/2)*dRmp_dp/dRy[i][j+1];

            // if(j!=0)
            //   dRc_dp+=(-1/h - dlog_etay/2)*dRmm_dp/dRy[i][j];

            // dRc_dp=2/3.0;

            Resid_p[i][j]=Rc;

            dp[i][j]=-theta_continuity*Rc/dRc_dp;

            if(!jacobi)
              p[i][j]+=dp[i][j];
            else
              p_new[i][j]=p[i][j]+dp[i][j];
          }

      if(sweep%output_interval==0)
        {
          std::stringstream ss;
          ss << "p_resid" << sweep;
          write_vtk(ss.str(),Nx,N,Resid_p);
        }

      /* Velocity Fix */

      double max_x_resid(0), max_y_resid(0), max_p_resid(0);

      for(int j=0;j<Ny;++j)
        for(int i=0;i<Nx;++i)
          {
            /* Fix vx */
            if(i>0)
              {
                if(!jacobi)
                  zx[i][j]+=(dp[i][j]-dp[i-1][j])/(h*dRx[i][j]);
                else
                  zx_new[i][j]+=(dp[i][j]-dp[i-1][j])/(h*dRx[i][j]);

                max_x_resid=std::max(std::fabs(Resid_x[i][j]),max_x_resid);
              }
            /* Fix vy */
            if(j>0)
              {
                if(!jacobi)
                  zy[i][j]+=(dp[i][j]-dp[i][j-1])/(h*dRy[i][j]);
                else
                  zy_new[i][j]+=(dp[i][j]-dp[i][j-1])/(h*dRy[i][j]);

                max_y_resid=std::max(fabs(Resid_y[i][j]),max_y_resid);
              }
            max_p_resid=std::max(fabs(Resid_p[i][j]),max_p_resid);
          }

      if(jacobi)
        for(int i=0;i<Nx+1;++i)
          for(int j=0;j<Ny+1;++j)
            {
              if(j<Ny)
                zx[i][j]=zx_new[i][j];
              if(i<Nx)
                zy[i][j]=zy_new[i][j];
              if(j<Ny && i<Nx)
                p[i][j]=p_new[i][j];
            }

      if(sweep%1000==0)
        {
          std::cout << "sweep "
                    << sweep << " "
                    << max_x_resid << " "
                    << max_y_resid << " "
                    << max_p_resid << " "
                    << "\n";
        }
      if(sweep%output_interval==0)
        {
          std::stringstream ss;
          ss << "zx" << sweep;
          write_vtk(ss.str(),Nx+1,N,zx);
          ss.str("");
          ss << "zy" << sweep;
          write_vtk(ss.str(),Nx,N,zy);
          ss.str("");
          ss << "p" << sweep;
          write_vtk(ss.str(),Nx,N,p);
        }

      if(max_x_resid < tolerance
         && max_y_resid < tolerance
         )
         // && max_p_resid < tolerance)
        {
          std::cout << "Solved "
                    << sweep << " "
                    << tolerance << " "
                    << max_x_resid << " "
                    << max_y_resid << " "
                    << max_p_resid << " "
                    << "\n";
          break;
        }
    }

  write_vtk("zx",Nx+1,N,zx);
  write_vtk("zy",Nx,N,zy);
  write_vtk("p",Nx,N,p);

  for(int i=0;i<Nx+1;++i)
    for(int j=0;j<Ny+1;++j)
      {
        if(j<Ny)
          zx[i][j]*=exp(-log_etax[i][j]);
        if(i<Nx)
          zy[i][j]*=exp(-log_etay[i][j]);
      }
  write_vtk("vx",Nx+1,N,zx);
  write_vtk("vy",Nx,N,zy);
}
