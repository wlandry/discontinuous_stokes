#ifndef GAMR_COEFFICIENT_HXX
#define GAMR_COEFFICIENT_HXX

class Coefficient
{
public:
  int i,j;
  double val;
  Coefficient(const int &I, const int &J, const double &V):
    i(I), j(J), val(V) {}
};

#endif
