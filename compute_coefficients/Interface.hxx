#ifndef GAMR_INTERFACE_HXX
#define GAMR_INTERFACE_HXX

#include "../constants.hxx"
#include "sign.hxx"
#include "FTensor.hpp"
#include <iostream>
#include <cassert>

class Interface
{
public:
  FTensor::Tensor1<double,2> grid_pos, dd_pos[2][2], sides_pos[2][2],
    corner_pos[2][2], anticorner_pos[2], dp_pos[2];
  bool intersect_dd[2][2], intersect_sides[2][2], intersect_corner[2][2],
    intersect_anticorner[2][2], intersect_dp[2];

  double eta_jump_dd[2][2], eta_jump_sides[2][2], eta_jump_corner[2][2],
    eta_jump_anticorner[2][2], eta_jump_dp[2];

  Interface(const int &d, const int &i, const int &j,
            const double log_etax[Nx+1][Ny],
            const double log_etay[Nx][Ny+1],
            const double eta_cell[Nx][Ny],
            const double distx[Nx+1][Ny], const double disty[Nx][Ny+1],
            const double dist_cell[Nx][Ny], const double dist_edge[Nx+1][Ny+1]);

  bool intersects() const
  {
    return intersect_dd[0][0] || intersect_dd[0][1]
      || intersect_dd[1][0] || intersect_dd[1][1]
      || intersect_sides[0][0] || intersect_sides[0][1]
      || intersect_sides[1][0] || intersect_sides[1][1]
      || intersect_corner[0][0] || intersect_corner[0][1]
      || intersect_corner[1][0] || intersect_corner[1][1]
      || intersect_anticorner[0][0] || intersect_anticorner[0][1]
      || intersect_anticorner[1][0] || intersect_anticorner[1][1]
      || intersect_dp[0] || intersect_dp[1];
  }

  static double intersection(const double &u_m, const double &u_0,
                             const double &u_p, const double &H)
  {
    /* This returns the lowest intersection between u_m and u_p */

    double du=(u_p-u_m)/2;
    double ddu=u_p-2*u_0+u_m;

    if(std::fabs(ddu)>1e-8*(std::fabs(u_p)+std::fabs(u_0)+std::fabs(u_m)))
      {
        assert(du*du-2*u_0*ddu>=0);
        double tmp=sqrt(du*du-2*u_0*ddu);

        double p=(-du+tmp)/ddu;
        double m=(-du-tmp)/ddu;

        if(m>-1)
          return m*H;
        return p*H;
      }
    else
      {
        return -H*u_0/du;
      }
  }

  template <int ny> void compute_dd(const int &d, const int &i, const int &j,
                                    const double log_eta[][ny],
                                    const double dist[][ny],
                                    const double dist_cell[Nx][Ny],
                                    const double dist_edge[Nx+1][Ny+1])
  {
    const int nx=Nx+1-d;

    /* TODO: fix if we have Neumann boundaries.  Also, this may cut
     off an interface at the boundary, because it assumes that if you
     are at i==0, then the interface does not cut below the i=0
     plane.  */
    intersect_dd[0][0]=(i!=0 && sign(dist[i][j])!=sign(dist[i-1][j]));
    intersect_dd[0][1]=(i!=nx-1 && sign(dist[i][j])!=sign(dist[i+1][j]));

    intersect_dd[1][0]=(j!=0 && sign(dist[i][j])!=sign(dist[i][j-1]));
    intersect_dd[1][1]=(j!=ny-1 && sign(dist[i][j])!=sign(dist[i][j+1]));

    /* TODO: Fix for variable viscosity */

    if(intersect_dd[0][0])
      eta_jump_dd[0][0]=exp(log_eta[i][j])-exp(log_eta[i-1][j]);
    if(intersect_dd[0][1])
      eta_jump_dd[0][1]=exp(log_eta[i][j])-exp(log_eta[i+1][j]);

    if(intersect_dd[1][0])
      eta_jump_dd[1][0]=exp(log_eta[i][j])-exp(log_eta[i][j-1]);
    if(intersect_dd[1][1])
      eta_jump_dd[1][1]=exp(log_eta[i][j])-exp(log_eta[i][j+1]);

    for(int pm=0;pm<2;++pm)
      {
        for(int dd=0;dd<2;++dd)
          {
            if(intersect_dd[dd][pm])
              {
                dd_pos[dd][pm]((dd+1)%2)=grid_pos((dd+1)%2);
                if(dd==d)
                  {
                    dd_pos[dd][pm](dd)=grid_pos(dd)
                      + (2*pm-1)*(intersection
                                  (dist[i][j],
                                   dist_cell[i+(pm-1)*(d==0)][j+(pm-1)*(d==1)],
                                   dist[i+(2*pm-1)*(dd==0)][j+(2*pm-1)*(dd==1)],
                                   h/2)
                                  + h/2);
                  }
                else
                  {
                    dd_pos[dd][pm](dd)=grid_pos(dd)
                      + (2*pm-1)*(intersection
                                  (dist[i][j],
                                   dist_edge[i+pm*(dd==0)][j+pm*(dd==1)],
                                   dist[i+(2*pm-1)*(dd==0)][j+(2*pm-1)*(dd==1)],
                                   h/2)
                                  +h/2);
                  }
              }
          }
      }
  }

  template <int ny> void compute_dp(const int &d, const int &i, const int &j,
                                    const double log_eta[][ny],
                                    const double eta_cell[Nx][Ny],
                                    const double dist[][ny],
                                    const double dist_cell[Nx][Ny])
  {
    for(int pm=0;pm<2;++pm)
      {
        int ip=i+(pm-1)*(d==0);
        int jp=j+(pm-1)*(d==1);

        intersect_dp[pm]=(sign(dist_cell[ip][jp])!=sign(dist[i][j]));

        if(intersect_dp[pm])
          {
            eta_jump_dp[pm]=exp(log_eta[i][j])-eta_cell[ip][jp];
            dp_pos[pm](d)=grid_pos(d)
              + (2*pm-1)*(intersection
                          (dist[i][j], dist_cell[ip][jp],
                           dist[i+(2*pm-1)*(d==0)][j+(2*pm-1)*(d==1)],h/2)
                          + h/2);
            dp_pos[pm]((d+1)%2)=grid_pos((d+1)%2);
          }
      }
  }

  template <int ny_x, int ny_y>
  void compute_xy(const int &d, const int &ii, const int &jj,
                  const double &log_eta_center,
                  const double log_etaY[][ny_y],
                  const double distX[][ny_x], const double distY[][ny_y],
                  const double dist_cell[Nx][Ny],
                  const double dist_edge[Nx+1][Ny+1])
  {
    const int i(ii-(d==0)), j(jj-(d==1));

    /* This has to be consistent with the checks for the second
       derivatives.  Otherwise, the stencil weights may get too large
       for off-center points */
    int center(sign(distX[ii][jj]));


    /* TODO: I do not quite like the explicit dependency on
       intersect_dd.  It seems like there could be cases where you do
       want to use the side rather than corner interface locations,
       but intersect_dd will not be true. */
    intersect_sides[0][0]=(center!=sign(distY[i][j])
                           && center!=sign(distY[i][j+1])
                           && intersect_dd[0][0]);
    intersect_sides[0][1]=(center!=sign(distY[i+1][j])
                           && center!=sign(distY[i+1][j+1])
                           && intersect_dd[0][1]);
    intersect_sides[1][0]=(center!=sign(distY[i][j])
                           && center!=sign(distY[i+1][j])
                           && intersect_dd[1][0]);
    intersect_sides[1][1]=(center!=sign(distY[i][j+1])
                           && center!=sign(distY[i+1][j+1])
                           && intersect_dd[1][1]);

    /* TODO: fix for variable viscosity */

    for(int dd=0;dd<2;++dd)
      for(int pm=0;pm<2;++pm)
        {
          if(intersect_sides[dd][pm])
            {
              int ip=i+pm*(dd==0);
              int jp=j+pm*(dd==1);

              eta_jump_sides[dd][pm]=
                exp(log_eta_center) - exp(log_etaY[ip][jp]);

              if(d==dd)
                {
                  sides_pos[dd][pm](dd)=grid_pos(dd) + (2*pm-1)
                    *(h/2 + intersection
                      (distX[ii][jj], dist_cell[ip][jp],
                       distX[ii+(2*pm-1)*(dd==0)][jj+(2*pm-1)*(dd==1)],
                       h/2));
                  sides_pos[dd][pm]((dd+1)%2)=grid_pos((dd+1)%2);
                }
              else
                {
                  int ie=ii+pm*(dd==0);
                  int je=jj+pm*(dd==1);

                  sides_pos[dd][pm](dd)=grid_pos(dd) + (2*pm-1)
                    *(h/2 + intersection
                      (distX[ii][jj], dist_edge[ie][je],
                       distX[ii+(2*pm-1)*(dd==0)][jj+(2*pm-1)*(dd==1)],
                       h/2));
                  sides_pos[dd][pm]((dd+1)%2)=grid_pos((dd+1)%2);
                }
            }
        }


    for(int n0=0;n0<2;++n0)
      for(int n1=0;n1<2;++n1)
        {
          intersect_corner[n0][n1]=(center!=sign(distY[i+n0][j+n1]));
          if(intersect_corner[n0][n1])
            {
              eta_jump_corner[n0][n1]=
                exp(log_eta_center)-exp(log_etaY[i+n0][j+n1]);

              FTensor::Tensor1<double,2> norm, tangent;
              norm(0)=distY[i+1][j]-distY[i][j];
              norm(1)=distY[i][j+1]-distY[i][j];
              tangent(0)=-norm(1);
              tangent(1)=norm(0);

              /* Use the intersection with the element boundary, not
                 the straight line from the center of the element to
                 the corner element.  This may cause problems when the
                 interface just barely skims the element.  We did it
                 this way because then the weighting of the stencil
                 always sums to zero.  May want to change this.  */
              /* TODO: this assumes straight boundaries */
              if(std::fabs(tangent(0))>std::fabs(tangent(1)))
                {
                  /* Top/Bottom */
                  corner_pos[n0][n1](0)=grid_pos(0)
                    + sign(n0-0.5)*h*(0.5
                                      - std::fabs(distY[i+n0][j+n1]
                                                  /(distY[i+(n0+1)%2][j+n1]
                                                    - distY[i+n0][j+n1])));
                  corner_pos[n0][n1](1)=grid_pos(1) + sign(n1-0.5)*h/2;
                }
              else
                {
                  /* Left/Right */
                  corner_pos[n0][n1](0)=grid_pos(0) + sign(n0-0.5)*h/2;

                  corner_pos[n0][n1](1)=grid_pos(1)
                    + sign(n1-0.5)*h*(0.5
                                      - std::fabs(distY[i+n0][j+n1]
                                                  /(distY[i+n0][j+(n1+1)%2]
                                                    - distY[i+n0][j+n1])));
                }
            }
        }
    for(int n0=0;n0<2;++n0)
      for(int n1=0;n1<2;++n1)
        {
          intersect_anticorner[n0][n1]=
            !intersect_corner[n0][n1]
            && intersect_corner[(n0+1)%2][n1]
            && intersect_corner[n0][(n1+1)%2]
            && intersect_corner[(n0+1)%2][(n1+1)%2];
          if(intersect_anticorner[n0][n1])
            {
              eta_jump_anticorner[n0][n1]=
                exp(log_eta_center)-exp(log_etaY[(n0+1)%2][n1]);

              /* Only need to compute positions for the diagonal
                 points, since the center point has already been
                 calculated for interface_corner. */
              double delta=distY[i+n0][j+n1]-distY[i+(n0+1)%2][j+n1];
              anticorner_pos[0](0)=grid_pos(0)
                +sign(n0-.5)*h*(0.5 - distY[i+n0][j+n1]/delta);
              anticorner_pos[0](1)=grid_pos(1)+sign(n1-.5)*h/2;

              delta=distY[i+n0][j+n1]-distY[i+n0][j+(n1+1)%2];
              anticorner_pos[1](0)=grid_pos(0)+sign(n0-.5)*h/2;
              anticorner_pos[1](1)=grid_pos(1)
                +sign(n1-.5)*h*(0.5 - distY[i+n0][j+n1]/delta);

              std::cerr << "Anticorners not tested yet"
                        << std::endl;
              abort();
            }
        }
  }
  
};

#endif
