#include "../Interface.hxx"

Interface::Interface(const int &d, const int &i, const int &j,
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1])
{
  switch(d)
    {
    case 0:
      grid_pos(0)=i*h;
      grid_pos(1)=(j+0.5)*h;
      break;
    case 1:
      grid_pos(0)=(i+0.5)*h;
      grid_pos(1)=j*h;
      break;
    default:
      abort();
    }

  if(d==0)
    {
      compute_dd(d,i,j,log_etax,distx,dist_cell,dist_edge);
      compute_dp(d,i,j,log_etax,eta_cell,distx,dist_cell);
      compute_xy(d,i,j,log_etax[i][j],log_etay,
                 distx,disty,dist_cell,dist_edge);
    }
  else
    {
      compute_dd(d,i,j,log_etay,disty,dist_cell,dist_edge);
      compute_dp(d,i,j,log_etay,eta_cell,disty,dist_cell);
      compute_xy(d,i,j,log_etay[i][j],log_etax,
                 disty,distx,dist_cell,dist_edge);
    }
}
