/* Compute only the parts of Ry that depend on zx, zy */

#include "../constants.hxx"
#include "compute_Cxyz.hxx"

double simplified_Ry(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1])
{
  double dzy_yy=
    (zy[i][j-1] - 2*zy[i][j] + zy[i][j+1])/(h*h);

  double dzx_yx=((zx[i+1][j] - zx[i+1][j-1])
                 - (zx[i][j] - zx[i][j-1]))/(h*h);

  double dzy_xx;
  if(i==0)
    {
      dzy_xx=(-zy[i][j] + zy[i+1][j])/(h*h);
    }
  else if(i==Nx-1)
    {
      dzy_xx=(zy[i-1][j] - zy[i][j])/(h*h);
    }
  else
    {
      dzy_xx=(zy[i-1][j] - 2*zy[i][j] + zy[i+1][j])/(h*h);
    }

  double Cy;
  compute_Cxyz(zx,zy,log_etax,log_etay,eta_cell,distx,disty,dist_cell,dist_edge,
               1,i,j,Cy);
               
  return 2*dzy_yy + dzy_xx + dzx_yx + Cy;
}
