#include "FTensor.hpp"
#include "../constants.hxx"

void compute_jumps(const double &eta_jump,
                   const FTensor::Tensor1<double,2> &v,
                   const FTensor::Tensor1<double,2> &dv,
                   const FTensor::Tensor2<double,2,2> &nt_to_xy,
                   FTensor::Tensor1<double,2> &z_jump,
                   FTensor::Tensor2<double,2,2> &dz_jump,
                   double &p_jump)
{
  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;
  const FTensor::Index<'c',2> c;
  const FTensor::Index<'d',2> d;
  const FTensor::Index<'e',2> e;
  const FTensor::Index<'f',2> f;

  z_jump(a)=eta_jump*v(b)*nt_to_xy(a,b);
  
  FTensor::Number<0> n;
  FTensor::Number<1> t;
  FTensor::Tensor2_symmetric<int,2> ddel(0,1,0);

  FTensor::Tensor2<double,2,2> dz_jump_nt;
  dz_jump_nt(a,n)=-eta_jump*dv(b)*ddel(a,b);
  dz_jump_nt(a,t)=eta_jump*dv(a);
  dz_jump(a,b)=dz_jump_nt(c,d)*nt_to_xy(a,c)*nt_to_xy(b,d);

  p_jump=-2*eta_jump*dv(t);
}

void compute_jumps(const double &eta_jump,
                   const FTensor::Tensor1<double,2> &v,
                   const FTensor::Tensor1<double,2> &dv,
                   const FTensor::Tensor2<double,2,2> &nt_to_xy,
                   FTensor::Tensor1<double,2> &z_jump,
                   FTensor::Tensor2<double,2,2> &dz_jump)
{
  double p_jump;
  compute_jumps(eta_jump,v,dv,nt_to_xy,z_jump,dz_jump,p_jump);
}
