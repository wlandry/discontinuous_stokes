#ifndef GAMR_COMPUTE_V_ON_INTERFACE_HXX
#define GAMR_COMPUTE_V_ON_INTERFACE_HXX

#include "FTensor.hpp"

void compute_v_on_interface(const double zx[Nx+1][Ny],
                            const double zy[Nx][Ny+1],
                            const double log_etax[Nx+1][Ny],
                            const double log_etay[Nx][Ny+1],
                            const double distx[Nx+1][Ny],
                            const double disty[Nx][Ny+1],
                            const double dist_cell[Nx][Ny],
                            const double dist_edge[Nx+1][Ny+1],
                            const FTensor::Tensor1<double,2> &pos,
                            const int &xyz,
                            FTensor::Tensor1<double,2> &v,
                            FTensor::Tensor1<double,2> &dv,
                            FTensor::Tensor2<double,2,2> &nt_to_xy);

#endif
