#include "../constants.hxx"
#include <iostream>
#include "compute_v_on_interface.hxx"
#include "FTensor.hpp"
#include "Interface.hxx"

void compute_jumps(const double &eta_jump,
                   const FTensor::Tensor1<double,2> &v,
                   const FTensor::Tensor1<double,2> &dv,
                   const FTensor::Tensor2<double,2,2> &nt_to_xy,
                   FTensor::Tensor1<double,2> &z_jump,
                   FTensor::Tensor2<double,2,2> &dz_jump);

void compute_jumps(const double &eta_jump,
                   const FTensor::Tensor1<double,2> &v,
                   const FTensor::Tensor1<double,2> &dv,
                   const FTensor::Tensor2<double,2,2> &nt_to_xy,
                   FTensor::Tensor1<double,2> &z_jump,
                   FTensor::Tensor2<double,2,2> &dz_jump,
                   double &p_jump);

void compute_Cxyz(const double zx[Nx+1][Ny],
                  const double zy[Nx][Ny+1],
                  const double log_etax[Nx+1][Ny],
                  const double log_etay[Nx][Ny+1],
                  const double eta_cell[Nx][Ny],
                  const double distx[Nx+1][Ny], const double disty[Nx][Ny+1],
                  const double dist_cell[Nx][Ny],
                  const double dist_edge[Nx+1][Ny+1],
                  const int &d,
                  const int &i, const int &j,
                  double &C)
{
  C=0;

  const Interface interface(d,i,j,log_etax,log_etay,eta_cell,
                            distx,disty,dist_cell,dist_edge);
  if(!interface.intersects())
    return;

  FTensor::Tensor1<double,2> pos;
  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;
  const FTensor::Index<'c',2> c;
  pos(a)=interface.grid_pos(a);

  FTensor::Tensor1<int,2> xyz(0,0);
  xyz(d)=1;
  bool try_corners(true);
  for(int pm=0;pm<2;++pm)
    {
      for(int dd=0;dd<2;++dd)
        {
          if(interface.intersect_dd[dd][pm])
            {

              /* xyz= the component we are correcting, Cx, Cy, or Cz.
                 dir=direction of 2nd derivative
                 dir2, dir3=components of mixed derivative we are correcting.
                 So for zy,xy with boundary in x, dir2=y, dir3=y. */
              FTensor::Tensor1<double,2> v, dv;
              FTensor::Tensor2<double,2,2> nt_to_xy;
              compute_v_on_interface(zx,zy,log_etax,log_etay,distx,disty,
                                     dist_cell,dist_edge,
                                     interface.dd_pos[dd][pm],d,
                                     v,dv,nt_to_xy);

              int sgn(sign(pos(dd) - interface.dd_pos[dd][pm](dd)));
              FTensor::Tensor1<double,2> z_jump;
              FTensor::Tensor2<double,2,2> dz_jump;
              double p_jump;

              compute_jumps(interface.eta_jump_dd[dd][pm],v,dv,nt_to_xy,
                            z_jump,dz_jump,p_jump);

              FTensor::Tensor1<double,2> dx(0,0);
              dx(dd)=pos(dd) - interface.dd_pos[dd][pm](dd) - h*sgn;
              double dz_dd_correction=
                xyz(a)*(z_jump(a) + dz_jump(a,b)*dx(b))/(h*h);

              const int dz_dd_factor(d==dd ? 2 : 1);

              C+=dz_dd_factor*dz_dd_correction;
            }
          if(interface.intersect_sides[dd][pm])
            {
              FTensor::Tensor1<double,2> v, dv;
              FTensor::Tensor2<double,2,2> nt_to_xy;
              compute_v_on_interface(zx,zy,log_etax,log_etay,distx,disty,
                                     dist_cell,dist_edge,
                                     interface.sides_pos[dd][pm],d,
                                     v,dv,nt_to_xy);

              int sgn(sign(pos(dd) - interface.sides_pos[dd][pm](dd)));
              FTensor::Tensor1<double,2> z_jump;
              FTensor::Tensor2<double,2,2> dz_jump;
              double p_jump;

              compute_jumps(interface.eta_jump_sides[dd][pm],v,dv,nt_to_xy,
                            z_jump,dz_jump,p_jump);

              try_corners=false;
              FTensor::Tensor1<int,2> dir2(0,0), dir3(0,0);
              dir2((d+1)%2)=1;
              dir3((dd+1)%2)=1;

              FTensor::Tensor1<double,2> dx(0,0);
              dx(dd)=pos(dd) - interface.sides_pos[dd][pm](dd) - (h/2)*sgn;

              C+=-sgn*(dz_jump(a,b)*dir2(a)*dir3(b))/h;
            }
        }
      if(interface.intersect_dp[pm])
        {
          FTensor::Tensor1<double,2> v, dv;
          FTensor::Tensor2<double,2,2> nt_to_xy;
          compute_v_on_interface(zx,zy,log_etax,log_etay,distx,disty,
                                 dist_cell,dist_edge,
                                 interface.dp_pos[pm],d,
                                 v,dv,nt_to_xy);

          int sgn(sign(pos(d) - interface.dp_pos[pm](d)));
          FTensor::Tensor1<double,2> z_jump;
          FTensor::Tensor2<double,2,2> dz_jump;
          double p_jump;

          compute_jumps(interface.eta_jump_dp[pm],v,dv,nt_to_xy,
                        z_jump,dz_jump,p_jump);

          FTensor::Tensor1<double,2> dx(0,0);
          dx(d)=pos(d) - interface.dp_pos[pm](d) - (h/2)*sgn;

          C+=sgn*p_jump/h;
        }
    }

  for(int dd=0;dd<2;++dd)
    for(int ee=0;ee<2;++ee)
      if(interface.intersect_anticorner[dd][ee])
        {
          try_corners=false;
          std::cerr << "Anticorners not implemented"
                    << std::endl;
          abort();
        }
  /* Handle corner cutting */
  if(try_corners)
    {
      FTensor::Tensor1<double,2> zyx(0,0);
      zyx((d+1)%2)=1;

      for(int dd=0;dd<2;++dd)
        for(int ee=0;ee<2;++ee)
          if(interface.intersect_corner[dd][ee])
            {
              FTensor::Tensor1<double,2> v, dv;
              FTensor::Tensor2<double,2,2> nt_to_xy;
              compute_v_on_interface(zx,zy,log_etax,log_etay,distx,disty,
                                     dist_cell,dist_edge,
                                     interface.corner_pos[dd][ee],d,
                                     v,dv,nt_to_xy);

              FTensor::Tensor1<double,2> z_jump;
              FTensor::Tensor2<double,2,2> dz_jump;

              compute_jumps(interface.eta_jump_corner[dd][ee],v,dv,nt_to_xy,
                            z_jump,dz_jump);

              FTensor::Tensor1<double,2> dx,yx_pos;

              yx_pos(0)=pos(0)+(dd-0.5)*h;
              yx_pos(1)=pos(1)+(ee-0.5)*h;

              dx(a)=yx_pos(a)-interface.corner_pos[dd][ee](a);

              C+=sign(dd-0.5)*sign(ee-0.5)*
                zyx(a)*(z_jump(a) + dz_jump(a,b)*dx(b))/(h*h);
            }
    }
}
