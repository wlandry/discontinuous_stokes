#ifndef GAMR_COMPUTE_1ST_DERIVS_HXX
#define GAMR_COMPUTE_1ST_DERIVS_HXX

#include <cassert>
#include "Valid.hxx"
#include <gsl/gsl_linalg.h>

template<int ny>
void compute_1st_derivs(const int &d, const Valid &V,
                        const int &d0,
                        const int &nx,
                        const double z[][ny],
                        const double log_eta[][ny],
                        FTensor::Tensor2<double,2,2> &dv_pm)
{
  /* TODO: this does not seem right at the boundary. */
  switch(d0)
    {
    case 0:
      if(V.i==nx-1 || V.i==-1)
        {
          dv_pm(d,d0)=0;
        }
      else
        {
          dv_pm(d,d0)=vel(z,log_eta,V.i+1,V.j) - vel(z,log_eta,V.i,V.j);
        }
      break;
    case 1:
      if(V.j==ny-1 || V.j==-1)
        {
          dv_pm(d,d0)=0;
        }
      else
        {
          dv_pm(d,d0)=vel(z,log_eta,V.i,V.j+1) - vel(z,log_eta,V.i,V.j);
        }
      break;
    default:
      abort();
    }
}

#endif
