#include "../constants.hxx"
#include "FTensor.hpp"

FTensor::Tensor1<double,2> compute_norm(const FTensor::Tensor1<double,2> &pos,
                                        const double distx[Nx+1][Ny],
                                        const double disty[Nx][Ny+1],
                                        const double dist_cell[Nx][Ny],
                                        const double dist_edge[Nx+1][Ny+1])
{
  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;
  int ix(pos(0)/h), iy(pos(0)/h - 0.5), jx(pos(1)/h - 0.5), jy(pos(1)/h);

  /* TODO: fix for curved interfaces, especially when the interface
     point is not along a grid coordinate. */
  FTensor::Tensor1<double,2> norm;
  if(iy<Ny)
    {
      norm(0)=(disty[iy+1][jy] - disty[iy][jy])/h;
      norm(1)=(disty[iy][jy+1] - disty[iy][jy])/h;
    }
  else
    {
      norm(0)=(distx[ix+1][jx] - distx[ix][jx])/h;
      norm(1)=(distx[ix][jx+1] - distx[ix][jx])/h;
    }
  norm(a)/=sqrt(norm(b)*norm(b));

  return norm;
}
