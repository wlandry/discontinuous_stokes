#include <algorithm>
#include <cmath>
#include "../constants.hxx"
#include <limits>
#include <iostream>
#include "FTensor.hpp"
#include "../compute_v_on_interface.hxx"
#include "vel.hxx"

FTensor::Tensor1<double,2> compute_norm(const FTensor::Tensor1<double,2> &pos,
                                        const double distx[Nx+1][Ny],
                                        const double disty[Nx][Ny+1],
                                        const double dist_cell[Nx][Ny],
                                        const double dist_edge[Nx+1][Ny+1]);

void compute_dv_dtt(const double zx[Nx+1][Ny],
                    const double zy[Nx][Ny+1],
                    const double log_etax[Nx+1][Ny],
                    const double log_etay[Nx][Ny+1],
                    const double distx[Nx+1][Ny],
                    const double disty[Nx][Ny+1],
                    const FTensor::Tensor1<double,2> &pos,
                    const FTensor::Tensor1<double,2> &norm,
                    const FTensor::Tensor1<double,2> &tangent,
                    const FTensor::Tensor2<double,2,2> &nt_to_xy,
                    FTensor::Tensor1<double,2> &dv,
                    FTensor::Tensor1<double,2> &v);

void compute_v_on_interface(const double zx[Nx+1][Ny],
                            const double zy[Nx][Ny+1],
                            const double log_etax[Nx+1][Ny],
                            const double log_etay[Nx][Ny+1],
                            const double distx[Nx+1][Ny],
                            const double disty[Nx][Ny+1],
                            const double dist_cell[Nx][Ny],
                            const double dist_edge[Nx+1][Ny+1],
                            const FTensor::Tensor1<double,2> &pos,
                            const int &xyz,
                            FTensor::Tensor1<double,2> &v,
                            FTensor::Tensor1<double,2> &dv,
                            FTensor::Tensor2<double,2,2> &nt_to_xy)
{
  const FTensor::Index<'a',2> a;

  FTensor::Tensor1<double,2> norm(compute_norm(pos,distx,disty,dist_cell,dist_edge));

  FTensor::Tensor1<double,2> tangent;
  tangent(0)=-norm(1);
  tangent(1)=norm(0);

  FTensor::Number<0> n;
  FTensor::Number<1> t;
  nt_to_xy(a,n)=norm(a);
  nt_to_xy(a,t)=tangent(a);

  compute_dv_dtt(zx,zy,log_etax,log_etay,distx,disty,pos,norm,tangent,nt_to_xy,
                 dv,v);
}
