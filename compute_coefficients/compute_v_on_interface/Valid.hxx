#ifndef GAMR_VALID_HXX
#define GAMR_VALID_HXX

#include <limits>

class Valid
{
public:
  int sign,i,j;
  bool valid;
  FTensor::Tensor1<double,2> diff;
  double r;

  Valid(): sign(0), valid(false), r(std::numeric_limits<double>::infinity()) {}
  Valid(const int &Sign, const bool &val, const int &I, const int &J,
        const FTensor::Tensor1<double,2> &Diff):
    sign(Sign), i(I), j(J), valid(val), diff(Diff),
    r(diff(0)*diff(0)+diff(1)*diff(1)) {}
  bool operator<(const Valid &v) const
  {
    return r<v.r;
  }
  bool operator>(const Valid &v) const
  {
    return r>v.r;
  }
};


#endif
