#ifndef GAMR_COMPUTE_VALUES_HXX
#define GAMR_COMPUTE_VALUES_HXX

#include <cassert>
#include "Valid.hxx"
#include <gsl/gsl_linalg.h>

template<int ny>
void compute_values(const int &d, const std::vector<Valid> &values,
                    const int &nx,
                    const double z[][ny],
                    const double log_eta[][ny],
                    const FTensor::Tensor1<double,2> &norm,
                    const double &signed_length,
                    FTensor::Tensor1<double,2> &v_pm)
{
  /* Create and invert the Vandermonde matrix */
  const unsigned int degree(3);
  assert(values.size()==3);
  double m[degree*degree], rhs[degree];
  for(unsigned int i=0;i<degree;++i)
    {
      rhs[i]=vel(z,log_eta,values[i].i,values[i].j);

      m[0+degree*i]=1;
      m[1+degree*i]=values[i].diff(0);
      m[2+degree*i]=values[i].diff(1);
    }

  gsl_matrix_view mv=gsl_matrix_view_array(m,degree,degree);
  gsl_vector_view rhsv=gsl_vector_view_array(rhs,degree);
  gsl_vector *x=gsl_vector_alloc(degree);
  int s;
  gsl_permutation *perm=gsl_permutation_alloc(degree);
  gsl_linalg_LU_decomp(&mv.matrix,perm,&s);
  gsl_linalg_LU_solve(&mv.matrix,perm,&rhsv.vector,x);

  double v=gsl_vector_get(x,0);

  FTensor::Tensor1<double,2> dv_xy;
  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;
  dv_xy(0)=gsl_vector_get(x,1);
  dv_xy(1)=gsl_vector_get(x,2);
  double dv=dv_xy(a)*norm(a);

  v_pm(d)=v - dv*signed_length;

  gsl_vector_free(x);
  gsl_permutation_free(perm);
}

#endif
