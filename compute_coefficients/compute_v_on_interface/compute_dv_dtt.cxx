#include "../constants.hxx"
#include <cmath>
#include "FTensor.hpp"
#include <vector>
#include "../sign.hxx"
#include "vel.hxx"
#include "compute_1st_derivs.hxx"
#include "compute_values.hxx"
#include <tuple>
#include <algorithm>

inline bool about_equal(const double &r1, const double &r2)
{
  return std::fabs(r1-r2)<1e-8*std::fabs(r1+r2);
}

void compute_dv_dtt(const double zx[Nx+1][Ny],
                    const double zy[Nx][Ny+1],
                    const double log_etax[Nx+1][Ny],
                    const double log_etay[Nx][Ny+1],
                    const double distx[Nx+1][Ny],
                    const double disty[Nx][Ny+1],
                    const FTensor::Tensor1<double,2> &pos,
                    const FTensor::Tensor1<double,2> &norm,
                    const FTensor::Tensor1<double,2> &tangent,
                    const FTensor::Tensor2<double,2,2> &nt_to_xy,
                    FTensor::Tensor1<double,2> &dv,
                    FTensor::Tensor1<double,2> &v)
{
  double length=h/std::max(std::abs(norm(0)),std::abs(norm(1)));

  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;
  const FTensor::Index<'c',2> c;
  FTensor::Tensor2<double,2,2> dv_pm[2];
  dv_pm[0](a,b)=0;
  dv_pm[1](a,b)=0;

  FTensor::Tensor1<double,2> v_pm[2];
  v_pm[0](a)=0;
  v_pm[1](a)=0;

  double eta_pm[2];

  /* First find which points can give valid derivatives */
  for(int d=0;d<2;++d)
    {
      FTensor::Tensor1<std::vector<Valid>,2> d_valid;
      d_valid(0).resize((2*max_r+1)*(2*max_r+1));
      d_valid(1).resize((2*max_r+1)*(2*max_r+1));

      std::vector<Valid> valid((2*max_r+1)*(2*max_r+1));

      int max_x(Nx-d), max_y(Ny+d-1), min_x(-d), min_y(d-1);
      double dx(d*h/2), dy((1-d)*h/2);

      int starti((pos(0)-dx)/h), startj((pos(1)-dy)/h);

      /* v */
      for(int i=std::max(starti-max_r,min_x);i<=std::min(starti+max_r,max_x);++i)
        for(int j=std::max(startj-max_r,0);j<=std::min(startj+max_r,max_y);++j)
          {
            FTensor::Tensor1<double,2> p(i*h+dx,j*h+dy), diff;
            diff(a)=p(a)-pos(a);

            if(d==0)
              {
                diff(a)-=length*norm(a)*sign(distx[i][j]);
                valid[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                  Valid(sign(distx[i][j]),true,i,j,diff);
              }
            else
              {
                diff(a)-=length*norm(a)*sign(disty[i][j]);
                if(i>min_x)
                  valid[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                    Valid(sign(disty[i][j]),true,i,j,diff);
              }
          }


      /* d/dx */
      for(int i=std::max(starti-max_r,min_x);i<=std::min(starti+max_r,max_x);++i)
        for(int j=std::max(startj-max_r,0);j<=std::min(startj+max_r,max_y);++j)
          {
            FTensor::Tensor1<double,2> p_d(i*h+dx+h/2,j*h+dy),
              d_diff;
            d_diff(a)=p_d(a)-pos(a);

            if(d==0)
              {
                /* dv */
                if(i<max_x)
                  {
                    d_diff(a)-=length*norm(a)*sign(distx[i][j]);
                    d_valid(0)[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                      Valid(sign(distx[i][j]),
                            sign(distx[i][j])==sign(distx[i+1][j]),
                            i,j,d_diff);
                  }
              }
            else
              {
                /* dv */
                bool v_d;
                /* TODO: This bothers me a little.  What if the
                   interface lies between the point and the boundary?
                   Shouldn't that make the derivative calculation
                   invalid? */
                if(i==min_x || i==max_x)
                  v_d=true;
                else
                  v_d=(sign(disty[i][j])==sign(disty[i+1][j]));

                d_diff(a)-=length*norm(a)*sign(disty[i][j]);
                d_valid(0)[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                  Valid(sign(disty[i][j]),v_d,i,j,d_diff);
              }
          }

      /* d/dy */
      for(int i=std::max(starti-max_r,0);i<=std::min(starti+max_r,max_x);++i)
        for(int j=std::max(startj-max_r,min_y);j<=std::min(startj+max_r,max_y);
            ++j)
          {
            FTensor::Tensor1<double,2> p_d(i*h+dx,j*h+dy+h/2), d_diff;
            d_diff(a)=p_d(a)-pos(a);

            if(d==0)
              {
                bool v_d;
                if(j==min_y || j==max_y)
                  v_d=true;
                else
                  v_d=(sign(distx[i][j])==sign(distx[i][j+1]));

                d_diff(a)-=length*norm(a)*sign(distx[i][j]);
                d_valid(1)[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                  Valid(sign(distx[i][j]),v_d,i,j,d_diff);
              }
            else
              {
                if(j<max_y)
                  {
                    d_diff(a)-=length*norm(a)*sign(disty[i][j]);
                    d_valid(1)[i-starti+max_r + (2*max_r+1)*(j-startj+max_r)]=
                      Valid(sign(disty[i][j]),
                            sign(disty[i][j])==sign(disty[i][j+1]),
                          i,j,d_diff);
                  }
              }
          }

      sort(d_valid(0).begin(), d_valid(0).end());
      sort(d_valid(1).begin(), d_valid(1).end());

      sort(valid.begin(), valid.end());

      /* Compute everything */
      /* v */

      std::vector<Valid> values[2];
      for(auto &V: valid)
        {
          if(values[0].size()==6 && values[1].size()==6)
            break;

          if(V.sign==0)
            continue;
          const int pm=(V.sign==-1 ? 0 : 1);
          switch(values[pm].size())
            {
            default:
              values[pm].push_back(V);
              break;
            case 3:
              break;
            case 2:
              if((V.i!=values[pm][0].i || V.i !=values[pm][1].i)
                 && (V.j!=values[pm][0].j || V.j !=values[pm][1].j))
                {
                  values[pm].push_back(V);
                  if(d==0)
                    {
                      eta_pm[pm]=exp(log_etax[values[pm][0].i][values[pm][0].j]);
                      compute_values(0,values[pm],Nx+1,zx,log_etax,
                                     norm,pm==0 ? -length : length,v_pm[pm]);
                    }
                  else
                    {
                      eta_pm[pm]=exp(log_etay[values[pm][0].i][values[pm][0].j]);
                      compute_values(1,values[pm],Nx,zy,log_etay,
                                     norm,pm==0 ? -length : length,v_pm[pm]);
                    }
                }
              break;
            }
        }

      for(int d0=0;d0<2;++d0)
        {
          /* First derivative dv */
          for(int pm=0; pm<2; ++pm)
            for(auto &V: d_valid(d0))
              {
                if(V.sign==2*pm-1)
                  {
                    switch(d)
                      {
                      case 0:
                        compute_1st_derivs(0,V,d0,Nx+1,zx,log_etax,dv_pm[pm]);
                        break;
                      case 1:
                        compute_1st_derivs(1,V,d0,Nx,zy,log_etay,dv_pm[pm]);
                        break;
                      default:
                        abort();
                        break;
                      }
                    break;
                  }
              }
        }
    }

  /* dv */
  FTensor::Tensor1<double,2> dv_xy(0,0);
  for(int pm=0;pm<2;++pm)
    dv_xy(a)+=dv_pm[pm](a,b)*tangent(b)*eta_pm[pm];

  dv(a)=nt_to_xy(b,a)*dv_xy(b)/(h*(eta_pm[0]+eta_pm[1]));

  /* v */
  FTensor::Tensor1<double,2> v_xy(0,0);
  for(int d=0;d<2;++d)
    v_xy(a)+=v_pm[d](a)*eta_pm[d];

  v(a)=nt_to_xy(b,a)*v_xy(b)/(eta_pm[0] + eta_pm[1]);
}

