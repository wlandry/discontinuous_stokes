#ifndef GAMR_VEL_HXX
#define GAMR_VEL_HXX

inline double analytic(const double x, const double y, const bool return_ux)
{
  const double eta_jump=max_eta-min_eta;

  double ux(1.1), ux_xp(1.2), ux_xxp(1.3), ux_xyp(1.4),
    ux_xm(1.5), ux_xxm(1.6), ux_xym(1.7),
    uy(2.1), uy_xp(2.2), uy_xxp(2.3), uy_xyp(2.4),
    uy_xm(2.5), uy_xxm(2.6), uy_xym(2.7);
  double ux_y(-(max_eta*uy_xp - min_eta*uy_xm)/eta_jump),
    ux_yy(-(max_eta*uy_xyp - min_eta*uy_xym)/eta_jump),
    uy_y(-(max_eta*ux_xp - min_eta*ux_xm)/eta_jump),
    uy_yy(-(max_eta*ux_xyp - min_eta*ux_xym)/eta_jump);

  // std::cout << "u y "
  //           << ux << " "
  //           << ux_y << " "
  //           << ux_yy << " "
  //           << uy << " "
  //           << uy_y << " "
  //           << uy_yy << " "
  //           << "\n";
  // exit(0);

  if(return_ux)
    {
      if(x>0)
        return ux + x*ux_xp + x*x*ux_xxp/2 + x*y*ux_xyp
          + y*ux_y + y*y*ux_yy/2;
      else
        return ux + x*ux_xm + x*x*ux_xxm/2 + x*y*ux_xym
          + y*ux_y + y*y*ux_yy/2;
    }
  else
    {
      if(x>0)
        return uy + x*uy_xp + x*x*uy_xxp/2 + x*y*uy_xyp
          + y*uy_y + y*y*uy_yy/2;
      else
        return uy + x*uy_xm + x*x*uy_xxm/2 + x*y*uy_xym
          + y*uy_y + y*y*uy_yy/2;
    }

}


template<int ny>
double vel(const double z[][ny], const double log_eta[][ny],
           const int i, const int j)
{
  // if(j>ny/8-4 && j<ny/8+4)
  //   {
  //     double x,y;
  //     if(ny%2==0)
  //       {
  //         x=i*h-middle;
  //         y=(j+0.5)*h-1.0/8;
  //         // y=j*h-1.0/8;
  //         return analytic(x,y,true);
  //       }
  //     else
  //       {
  //         x=(i+0.5)*h-middle;
  //         y=j*h-1.0/8;
  //         // y=(j-0.5)*h-1.0/8;
  //         return analytic(x,y,false);
  //       }
  //   }

  return z[i][j]*std::exp(-log_eta[i][j]);
}

#endif
