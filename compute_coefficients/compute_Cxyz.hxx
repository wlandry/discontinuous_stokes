#ifndef GAMR_COMPUTE_CXYZ_HXX
#define GAMR_COMPUTE_CXYZ_HXX

#include "../constants.hxx"
void compute_Cxyz(const double zx[Nx+1][Ny],
                  const double zy[Nx][Ny+1],
                  const double log_etax[Nx+1][Ny],
                  const double log_etay[Nx][Ny+1],
                  const double eta_cell[Nx][Ny],
                  const double distx[Nx+1][Ny], const double disty[Nx][Ny+1],
                  const double dist_cell[Nx][Ny],
                  const double dist_edge[Nx+1][Ny+1],
                  const int &d,
                  const int &i, const int &j,
                  double &C);


#endif
