/* Compute only the parts of Rp that depend on zx, zy */

#include "../constants.hxx"

extern void compute_Cp(const double zx[Nx+1][Ny],
                       const double zy[Nx][Ny+1],
                       const double log_etax[Nx+1][Ny],
                       const double log_etay[Nx][Ny+1],
                       const double eta_cell[Nx][Ny],
                       const double distx[Nx+1][Ny],
                       const double disty[Nx][Ny+1],
                       const double dist_cell[Nx][Ny],
                       const double dist_edge[Nx+1][Ny+1],
                       const int &i, const int &j,
                       double &Cp);

double simplified_Rp(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1])
{
  double dzx_x=(zx[i+1][j] - zx[i][j])/h;
  double dzy_y=(zy[i][j+1] - zy[i][j])/h;

  double Cp;
  compute_Cp(zx,zy,log_etax,log_etay,eta_cell,distx,disty,dist_cell,dist_edge,i,j,Cp);
  return dzx_x + dzy_y + Cp;
}
