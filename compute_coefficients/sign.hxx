#ifndef GAMR_SIGN_HXX
#define GAMR_SIGN_HXX

template <typename T> int sign(T val) {
    return (val > T(0)) - (val < T(0));
}

#endif
