#include "../constants.hxx"
#include <iostream>
#include "compute_v_on_interface.hxx"
#include "FTensor.hpp"
#include "sign.hxx"
#include "Interface.hxx"

void compute_jumps(const double &eta_jump,
                   const FTensor::Tensor1<double,2> &v,
                   const FTensor::Tensor1<double,2> &dv,
                   const FTensor::Tensor2<double,2,2> &nt_to_xy,
                   FTensor::Tensor1<double,2> &z_jump,
                   FTensor::Tensor2<double,2,2> &dz_jump);

void compute_Cp(const double zx[Nx+1][Ny],
                const double zy[Nx][Ny+1],
                const double log_etax[Nx+1][Ny],
                const double log_etay[Nx][Ny+1],
                const double eta_cell[Nx][Ny],
                const double distx[Nx+1][Ny], const double disty[Nx][Ny+1],
                const double dist_cell[Nx][Ny],
                const double dist_edge[Nx+1][Ny+1],
                const int &i, const int &j,
                double &Cp)
{
  Cp=0;

  const int sgn(sign(dist_cell[i][j]));
  bool intersects[][2]={{sign(distx[i][j])!=sgn,
                         sign(distx[i+1][j])!=sgn},
                        {sign(disty[i][j])!=sgn,
                         sign(disty[i][j+1])!=sgn}};

  FTensor::Tensor1<double,2> pos((i+0.5)*h,(j+0.5)*h);
  
  FTensor::Tensor1<double,2> interface_pos[2][2];
  for(int pm=0;pm<2;++pm)
    {
      if(intersects[0][pm])
        {
          interface_pos[0][pm](0)=pos(0)
            + (1-2*pm)*Interface::intersection(distx[i+pm][j],dist_cell[i][j],
                                               distx[i+1-pm][j],h/2);

          interface_pos[0][pm](1)=pos(1);
        }
      if(intersects[1][pm])
        {
          interface_pos[1][pm](0)=pos(0);

          interface_pos[1][pm](1)=pos(1)
            + (1-2*pm)*Interface::intersection(disty[i][j+pm],dist_cell[i][j],
                                               disty[i][j+1-pm],h/2);
        }
    }

  for(int dd=0;dd<2;++dd)
    for(int pm=0;pm<2;++pm)
      if(intersects[dd][pm])
        {
          FTensor::Tensor1<double,2> v, dv;
          FTensor::Tensor1<int,2> dir(0,0);
          FTensor::Tensor2<double,2,2> nt_to_xy;
          const int sgn(sign(pos(dd)-interface_pos[dd][pm](dd)));
          dir(dd)=sgn;
          compute_v_on_interface(zx,zy,log_etax,log_etay,distx,disty,
                                 dist_cell,dist_edge,
                                 interface_pos[dd][pm],dd,v,dv,nt_to_xy);

          FTensor::Tensor1<double,2> z_jump;
          FTensor::Tensor2<double,2,2> dz_jump;

          double eta_jump;
          switch(dd)
            {
            case 0:
              eta_jump=eta_cell[i][j]-exp(log_etax[i+pm][j]);
              break;
            case 1:
              eta_jump=eta_cell[i][j]-exp(log_etay[i][j+pm]);
              break;
            default:
              abort();
              break;
            }
          compute_jumps(eta_jump,v,dv,nt_to_xy,z_jump,dz_jump);

          double dx=std::fabs(pos(dd)-interface_pos[dd][pm](dd) - (h/2)*sgn);

          const FTensor::Index<'a',2> a;
          const FTensor::Index<'b',2> b;

          Cp-=dir(a)*(z_jump(a) - dx*dz_jump(a,b)*dir(b))/h;
        }
}
