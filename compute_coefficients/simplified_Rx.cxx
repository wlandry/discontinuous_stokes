/* Compute only the parts of Rx that depend on zx, zy */

#include "../constants.hxx"
#include "compute_Cxyz.hxx"

double simplified_Rx(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1])
{
  double dzx_xx=
    (zx[i-1][j] - 2*zx[i][j] + zx[i+1][j])/(h*h);

  double dzy_xy=((zy[i][j+1] - zy[i-1][j+1])
                 - (zy[i][j] - zy[i-1][j]))/(h*h);

  double dzx_yy;
  if(j==0)
    {
      dzx_yy=(-zx[i][j] + zx[i][j+1])/(h*h);
    }
  else if(j==Ny-1)
    {
      dzx_yy=(zx[i][j-1] - zx[i][j])/(h*h);
    }
  else
    {
      dzx_yy=(zx[i][j-1] - 2*zx[i][j] + zx[i][j+1])/(h*h);
    }

  double Cx;
  compute_Cxyz(zx,zy,log_etax,log_etay,eta_cell,distx,disty,dist_cell,dist_edge,
               0,i,j,Cx);

  return 2*dzx_xx + dzx_yy + dzy_xy + Cx;
}
