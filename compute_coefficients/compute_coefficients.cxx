#include "../constants.hxx"
#include "../Coefficient.hxx"
#include <vector>
#include <algorithm>
#include "FTensor.hpp"

#include <iostream>

template< typename T, class Allocator >
void shrink_capacity(std::vector<T,Allocator>& v)
{
   std::vector<T,Allocator>(v.begin(),v.end()).swap(v);
}

double simplified_Rx(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1]);

double simplified_Ry(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1]);

double simplified_Rp(const int &i, const int &j,
                     const double zx[Nx+1][Ny],
                     const double zy[Nx][Ny+1],
                     const double log_etax[Nx+1][Ny],
                     const double log_etay[Nx][Ny+1],
                     const double eta_cell[Nx][Ny],
                     const double fx[Nx+1][Ny],
                     const double fy[Nx][Ny+1],
                     const double distx[Nx+1][Ny],
                     const double disty[Nx][Ny+1],
                     const double dist_cell[Nx][Ny],
                     const double dist_edge[Nx+1][Ny+1]);

void compute_coefficients(const double log_etax[Nx+1][Ny],
                          const double log_etay[Nx][Ny+1],
                          const double eta_cell[Nx][Ny],
                          const double distx[Nx+1][Ny],
                          const double disty[Nx][Ny+1],
                          const double dist_cell[Nx][Ny],
                          const double dist_edge[Nx+1][Ny+1],
                          std::vector<Coefficient> Cfx[2][Nx+1][Ny],
                          std::vector<Coefficient> Cfy[2][Nx][Ny+1],
                          std::vector<Coefficient> Cfp[2][Nx][Ny],
                          double dRx[Nx+1][Ny],
                          double dRy[Nx][Ny+1])
{
  double zx[Nx+1][Ny], zy[Nx][Ny+1], p[Nx][Ny];
  double fx[Nx+1][Ny], fy[Nx][Ny+1];

  for(auto &I: zx)
    for(auto &J: I)
      J=0;

  for(auto &I: zy)
    for(auto &J: I)
      J=0;

  for(auto &I: p)
    for(auto &J: I)
      J=0;

  for(auto &I: fx)
    for(auto &J: I)
      J=0;

  for(auto &I: fy)
    for(auto &J: I)
      J=0;

  const int max_rp1(max_r+1);
  FTensor::Tensor2<int,2,2> max_ij(Nx,Ny-1,Nx-1,Ny);
  for(int i=1;i<Nx;++i)
    {
    for(int j=0;j<Ny;++j)
      {
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(0,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(0,1));
              ++dj)
            {
              zx[di][dj]=1;
              double R=simplified_Rx(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfx[0][i][j].push_back(Coefficient(di,dj,R));

              if(di==i && dj==j)
                dRx[i][j]=R;
              zx[di][dj]=0;
            }        
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(1,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(1,1));
              ++dj)
            {
              zy[di][dj]=1;
              double R=simplified_Rx(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfx[1][i][j].push_back(Coefficient(di,dj,R));
              zy[di][dj]=0;
            }        

        /* TODO: figure out whether I really need to do this and
           whether this is a good place to do it?  Maybe it should be
           done after everything has been calculated? */
        shrink_capacity(Cfx[0][i][j]);
        shrink_capacity(Cfx[1][i][j]);
      }
    }
  for(int i=0;i<Nx;++i)
    for(int j=1;j<Ny;++j)
      {
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(0,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(0,1));
              ++dj)
            {
              zx[di][dj]=1;
              double R=simplified_Ry(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfy[0][i][j].push_back(Coefficient(di,dj,R));
              zx[di][dj]=0;
            }        
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(1,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(1,1));
              ++dj)
            {
              zy[di][dj]=1;
              double R=simplified_Ry(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfy[1][i][j].push_back(Coefficient(di,dj,R));

              if(di==i && dj==j)
                dRy[i][j]=R;
              zy[di][dj]=0;
            }        
        shrink_capacity(Cfy[0][i][j]);
        shrink_capacity(Cfy[1][i][j]);
      }

  for(int i=0;i<Nx;++i)
    for(int j=0;j<Ny;++j)
      {
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(0,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(0,1));
              ++dj)
            {
              zx[di][dj]=1;
              double R=simplified_Rp(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfp[0][i][j].push_back(Coefficient(di,dj,R));
              zx[di][dj]=0;
            }        
        for(int di=std::max(i-max_rp1,0);di<=std::min(i+max_rp1,max_ij(1,0));
            ++di)
          for(int dj=std::max(j-max_rp1,0);dj<=std::min(j+max_rp1,max_ij(1,1));
              ++dj)
            {
              zy[di][dj]=1;
              double R=simplified_Rp(i,j,zx,zy,log_etax,log_etay,eta_cell,fx,fy,
                                     distx,disty,dist_cell,dist_edge);
              if(R!=0)
                Cfp[1][i][j].push_back(Coefficient(di,dj,R));
              zy[di][dj]=0;
            }        
        shrink_capacity(Cfp[0][i][j]);
        shrink_capacity(Cfp[1][i][j]);
      }
}

