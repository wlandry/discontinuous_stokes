#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include <fstream>

template<int NY>
void write_vtk(const std::string &name, int NX, int N, const double data[][NY])
{
  boost::filesystem::create_directory("output");
  boost::filesystem::ofstream initial("output/"+name+".vtk");

  const double spacing(1.0/N);
  const double origin[]={(NX==N ? spacing/2:0),(NY==2*N ? spacing/2:0)};

  initial << "# vtk DataFile Version 2.0\n"
          << name << "\n"
          << "ASCII\n"
          << "DATASET STRUCTURED_POINTS\n"
          << "DIMENSIONS "
          << NX << " "
          << NY << " "
          << "1\n"
          << "ORIGIN "
          << origin[0] << " "
          << origin[1] << " "
          << "0 \n"
          << "SPACING "
          << spacing << " "
          << spacing << " "
          << spacing << "\n"
          << "POINT_DATA "
          << NX*NY << "\n"
          << "SCALARS " << name << " double 1\n"
          << "LOOKUP_TABLE default\n";

  for(int j=0;j<NY;++j)
    for(int i=0;i<NX;++i)
      {
        initial << data[i][j] << "\n";
      }
}
