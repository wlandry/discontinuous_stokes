#include <complex>
#include "Model.hxx"
#include "constants.hxx"
#include "write_vtk.hxx"
#include "FTensor.hpp"

#include <cmath>
#include <gsl/gsl_linalg.h>

void initial(const Model &model, double zx[Nx+1][Ny], double zy[Nx][Ny+1],
             double log_etax[Nx+1][Ny], double log_etay[Nx][Ny+1],
             double eta_cell[Nx][Ny],
             double p[Nx][Ny], double fx[Nx+1][Ny], double fy[Nx][Ny+1],
             double distx[Nx+1][Ny], double disty[Nx][Ny+1],
             double dist_cell[Nx][Ny],
             double dist_edge[Nx+1][Ny+1])
{
  const double A(min_eta*(max_eta-min_eta)/(max_eta+min_eta));
  std::complex<double> phi, psi, dphi, v;

  const double pi(std::atan(1.0)*4);
  const double kx0(pi*middle), kx1(pi*(middle-1));
  const double cosh0(std::cosh(kx0)), sinh0(std::sinh(kx0)),
    cos0(std::cos(kx0)), sin0(std::sin(kx0)),
    cosh1(std::cosh(kx1)), sinh1(std::sinh(kx1)),
    cos1(std::cos(kx1)), sin1(std::sin(kx1));

  const double eta0(min_eta), eta1(max_eta), rho(0.5);

  double m[]={
    -kx0*cosh0, sinh0-kx0*cosh0, kx1*cosh1, -(sinh1-kx1*cosh1),
    cosh0+kx0*sinh0, kx0*sinh0, -(cosh1+kx1*sinh1), -kx1*sinh1,
    -kx0*sinh0*eta0, (cosh0-kx0*sinh0)*eta0,
    kx1*sinh1*eta1, -(cosh1-kx1*sinh1)*eta1,
    (-sinh0-kx0*cosh0)*eta0, -kx0*cosh0*eta0,
    (sinh1+kx1*cosh1)*eta1, kx1*cosh1*eta1};

  double rhs[]={
    ((kx0*cosh0 - sin0)/eta0 + (kx1*cosh1 - sin1)/eta1)*rho/2,
    ((cos0 - cosh0 - kx0*sinh0)/eta0 + (cos1 - cosh1 - kx1*sinh1)/eta1)*rho/2,
    (kx0*sinh0 + kx1*sinh1)*rho/2,
    ((sinh0 + kx0*cosh0) + (sinh1 + kx1*cosh1))*rho/2};

  gsl_matrix_view mv=gsl_matrix_view_array(m,4,4);
  gsl_vector_view rhsv=gsl_vector_view_array(rhs,4);
  gsl_vector *x=gsl_vector_alloc(4);
  int s;
  gsl_permutation *perm=gsl_permutation_alloc(4);
  gsl_linalg_LU_decomp(&mv.matrix,perm,&s);
  gsl_linalg_LU_solve(&mv.matrix,perm,&rhsv.vector,x);

  double v0(gsl_vector_get(x,0)), tau_eta_0(gsl_vector_get(x,1)),
    v1(gsl_vector_get(x,2)), tau_eta_1(gsl_vector_get(x,3));
  gsl_vector_free(x);
  gsl_permutation_free(perm);


  // const double theta(0.5);
  // const double theta(pi);
  const double theta(0);
  FTensor::Tensor2<double,2,2>
    rot(std::cos(theta),std::sin(theta),-std::sin(theta),std::cos(theta));
  FTensor::Tensor1<double,2> offset(0.5,0.5);
  const FTensor::Index<'a',2> a;
  const FTensor::Index<'b',2> b;

  for(int i=0;i<Nx+1;++i)
    for(int j=0;j<Ny+1;++j)
      {
        {
          FTensor::Tensor1<double,2> coord(i*h,j*h), xy;
          xy(a)=rot(a,b)*(coord(b)-offset(b)) + offset(a);
          double x(xy(0)), y(xy(1)), r2(x*x+y*y);

          switch(model)
            {
            case Model::inclusion:
              dist_edge[i][j]=std::sqrt(r2)-sqrt(r2_inclusion);
              break;
            case Model::solCx:
              dist_edge[i][j]=x-middle;
              break;
            default:
              abort();
              break;
            }
        }

        if(i<Nx && j<Ny)
          {
            FTensor::Tensor1<double,2> coord((i+0.5)*h,(j+0.5)*h), xy;
            xy(a)=rot(a,b)*(coord(b)-offset(b)) + offset(a);
            double x(xy(0)), y(xy(1)), r2(x*x+y*y);
            switch(model)
              {
              case Model::inclusion:
                p[i][j]=0;
                if(r2>=r2_inclusion)
                  {
                    p[i][j]=-2*A*(r2_inclusion/r2)*2*epsilon*(2*x*x/r2 -1);
                    eta_cell[i][j]=min_eta;
                  }
                else
                  {
                    eta_cell[i][j]=max_eta;
                  }
                dist_cell[i][j]=std::sqrt(r2)-std::sqrt(r2_inclusion);
                break;
              case Model::solCx:
                {
                  double kx, v, tau_eta, eta, sign;
                  if(x<middle)
                    {
                      kx=pi*x;
                      v=v0;
                      tau_eta=tau_eta_0;
                      eta=min_eta;
                      sign=1;
                    }
                  else
                    {
                      kx=pi*(x-1);
                      v=v1;
                      tau_eta=tau_eta_1;
                      eta=max_eta;
                      sign=-1;
                    }                      
                  const double csh(std::cosh(kx)), cs(std::cos(kx)),
                    snh(std::sinh(kx));

                  double tau_xx=2*pi*(-v*eta*kx*snh + tau_eta*eta*(csh-kx*snh)
                                      - sign*kx*snh*rho/2);

                  double dvx_x=(-v*(csh + kx*snh) + tau_eta*(-kx*snh)
                                - sign*(kx*snh + csh - cs)*rho/(2*eta));

                  p[i][j]=(2*pi*eta*dvx_x - tau_xx)*std::cos(pi*y);
                  dist_cell[i][j]=x-middle;
                  eta_cell[i][j]=eta;
                }
                break;
              default:
                abort();
                break;
              }
          }
        if(j<Ny)
          {
            FTensor::Tensor1<double,2> coord(i*h,(j+0.5)*h), xy;
            xy(a)=rot(a,b)*(coord(b)-offset(b)) + offset(a);
            double x(xy(0)), y(xy(1)), r2(x*x+y*y);
            zx[i][j]=0;
            fx[i][j]=0;
            switch(model)
              {
              case Model::solKz:
                log_etax[i][j]=y*log_max_eta;
                break;
              case Model::solCx:
                {
                  FTensor::Tensor1<double,2> f(0,pi*pi*sin(pi*y)*cos(pi*x));
                  fx[i][j]=f(b)*rot(b,0);
                }
                distx[i][j]=x-middle;
                if(x>middle)
                  {
                    log_etax[i][j]=log_max_eta;
                  }
                else
                  {
                    log_etax[i][j]=log_min_eta;
                  }
                {
                  double kx, v, tau_eta, eta, sign;
                  if(x<middle)
                    {
                      kx=pi*x;
                      v=v0;
                      tau_eta=tau_eta_0;
                      eta=min_eta;
                      sign=1;
                    }
                  else
                    {
                      kx=pi*(x-1);
                      v=v1;
                      tau_eta=tau_eta_1;
                      eta=max_eta;
                      sign=-1;
                    }                      
                  const double csh(std::cosh(kx)), snh(std::sinh(kx)),
                    sn(std::sin(kx)), cs(std::cos(kx));

                  FTensor::Tensor1<double,2>
                    z((tau_eta*snh - (v+tau_eta)*kx*csh
                       - sign*rho*(kx*csh/2 - sn/2)/eta)
                      *std::cos(pi*y)*eta,
                      (v*csh + (v+tau_eta)*kx*snh
                       - sign*rho*(cs - csh - kx*snh)/(2*eta))
                      *std::sin(pi*y)*eta);
                  zx[i][j]=rot(b,0)*z(b);
                }
                break;
              case Model::sinker:
                if(x>.4 && x < .6 && y>.2 && y<.4)
                  {
                    log_etax[i][j]=log_max_eta;
                  }
                else
                  {
                    log_etax[i][j]=log_min_eta;
                  }
                break;
              case Model::inclusion:
                std::complex<double> z(x,y);
                if(r2<r2_inclusion)
                  {
                    log_etax[i][j]=log_max_eta;
                    phi=0;
                    dphi=0;
                    psi=-4*epsilon*(max_eta*min_eta/(min_eta+max_eta))*z;
                  }
                else
                  {
                    log_etax[i][j]=log_min_eta;
                    phi=-2*epsilon*A*r2_inclusion/z;
                    dphi=-phi/z;
                    psi=-2*epsilon*(min_eta*z+A*r2_inclusion*r2_inclusion/(z*z*z));
                  }
                v=(phi - z*conj(dphi) - conj(psi))/2.0;
                zx[i][j]=v.real();
                distx[i][j]=std::sqrt(r2)-std::sqrt(r2_inclusion);
                break;
              }
          }

        if(i<Nx)
          {
            FTensor::Tensor1<double,2> coord((i+0.5)*h, j*h), xy;
            xy(a)=rot(a,b)*(coord(b)-offset(b)) + offset(a);
            double x(xy(0)), y(xy(1)), r2(x*x+y*y);
            zy[i][j]=0;

            switch(model)
              {
              case Model::solKz:
                fy[i][j]=sin(pi*y)*cos(pi*x);
                log_etay[i][j]=y*log_max_eta;
                break;
              case Model::solCx:
                {
                  FTensor::Tensor1<double,2> f(0,pi*pi*sin(pi*y)*cos(pi*x));
                  fy[i][j]=f(b)*rot(b,1);
                }
                disty[i][j]=x-middle;
                if(x>middle)
                  {
                    log_etay[i][j]=log_max_eta;
                  }
                else
                  {
                    log_etay[i][j]=log_min_eta;
                  }
                {
                  double kx, v, tau_eta, eta, sign;
                  if(x<middle)
                    {
                      kx=pi*x;
                      v=v0;
                      tau_eta=tau_eta_0;
                      eta=min_eta;
                      sign=1;
                    }
                  else
                    {
                      kx=pi*(x-1);
                      v=v1;
                      tau_eta=tau_eta_1;
                      eta=max_eta;
                      sign=-1;
                    }                      
                  const double snh(std::sinh(kx)), csh(std::cosh(kx)),
                    sn(std::sin(kx)), cs(std::cos(kx));
                    
                  FTensor::Tensor1<double,2>
                    z((tau_eta*snh - (v+tau_eta)*kx*csh
                       - sign*rho*(kx*csh/2 - sn/2)/eta)
                      *std::cos(pi*y)*eta,
                      (v*csh + (v+tau_eta)*kx*snh
                       - sign*rho*(cs - csh - kx*snh)/(2*eta))
                      *std::sin(pi*y)*eta);
                  zy[i][j]=rot(b,1)*z(b);
                }
                break;
              case Model::sinker:
                if(x>.4 && x < .6 && y>.2 && y<.4)
                  {
                    log_etay[i][j]=log_max_eta;
                    fy[i][j]=1;
                  }
                else
                  {
                    log_etay[i][j]=log_min_eta;
                    fy[i][j]=0;
                  }
                break;
              case Model::inclusion:
                std::complex<double> z(x,y);
                if(r2<r2_inclusion)
                  {
                    log_etay[i][j]=log_max_eta;
                    phi=0;
                    dphi=0;
                    psi=-4*epsilon*(max_eta*min_eta/(min_eta+max_eta))*z;
                  }
                else
                  {
                    log_etay[i][j]=log_min_eta;
                    phi=-2*epsilon*A*r2_inclusion/z;
                    dphi=-phi/z;
                    psi=-2*epsilon
                      * (min_eta*z + A*r2_inclusion*r2_inclusion/(z*z*z));
                  }
                v=(phi - z*conj(dphi) - conj(psi))/2.0;
                zy[i][j]=v.imag();
                
                disty[i][j]=std::sqrt(r2)-std::sqrt(r2_inclusion);
                break;
              }
          }
      }
  write_vtk("p_initial",Nx,N,p);
  write_vtk("zx_initial",Nx+1,N,zx);
  write_vtk("zy_initial",Nx,N,zy);
  write_vtk("distx",Nx+1,N,distx);
  write_vtk("disty",Nx,N,disty);
  write_vtk("dist_cell",Nx,N,dist_cell);
  write_vtk("dist_edge",Nx+1,N,dist_edge);
  write_vtk("log_etax",Nx+1,N,log_etax);
  write_vtk("log_etay",Nx,N,log_etay);
}
